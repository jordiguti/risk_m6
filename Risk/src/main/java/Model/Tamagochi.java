package Model;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

//Anotacio per a marcar la classe com a entitat
@Entity
// Anotacio per indicar el nom de la taula
@Table(name = "tamagochi")
public class Tamagochi {

	// @Id ens indica que aquest atribut es una clau primaria
	@Id
	// @GeneratedValue indica a Hibernate com ha de generar la ID
	@GeneratedValue(strategy = GenerationType.AUTO)
	// @Column ens permet modificar paràmetres de la columna
	@Column(name = "id_tamagochi")
	private int id;
	@Column(name = "nom", length = 50, nullable = false)
	private String nom;
	@Column(name = "descripcio", length = 50, nullable = false)
	private String descripcio;
	@Column(name = "gana", columnDefinition = "double(6,2)")
	private double gana = 50.00;
	@Column(name = "viu")
	private Boolean viu;
	@Column(name = "felicitat")
	private int felicitat = 50;
	@Column(name = "etapa", nullable = false)
	@Enumerated(EnumType.STRING)
	private Etapa etapa;
	@Column(name = "dataNaixement")
	private LocalDateTime dataNaixement = LocalDateTime.now();
	
	
	
	
	
	
	
	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn(name="id_aliment")
	private Aliment aliment;
	
	@OneToOne(mappedBy = "tamagochi")
	private Joguina joguina;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="tamagotchi_amic", joinColumns = @JoinColumn(name="id_tamagochi"), inverseJoinColumns = @JoinColumn(name="amic_id"))
	private Set<Tamagochi> amics = new HashSet<Tamagochi>();
	
	@ManyToMany(mappedBy = "amics", cascade=CascadeType.ALL)
	private Set<Tamagochi> amicDe = new HashSet<Tamagochi>();


	public Aliment getAliment() {
		return aliment;
	}
	public void setAliment(Aliment aliment) {
		this.aliment = aliment;
	}
	public Joguina getJoguina() {
		return joguina;
	}
	public void setJoguina(Joguina joguina) {
		this.joguina = joguina;
	}
	public Set<Tamagochi> getAmics() {
		return amics;
	}
	public void setAmics(Set<Tamagochi> amics) {
		this.amics = amics;
	}
	public Set<Tamagochi> getAmicDe() {
		return amicDe;
	}
	public void setAmicDe(Set<Tamagochi> amicDe) {
		this.amicDe = amicDe;
	}
	
	
	
	
	
	

	public Tamagochi() {
		super();
	}

	public Tamagochi(int id, String nom, String descripcio, double gana, Boolean viu, int felicitat, Etapa etapa,
			LocalDateTime dataNaixement) {
		super();
		this.id = id;
		this.nom = nom;
		this.descripcio = descripcio;
		this.gana = gana;
		this.viu = viu;
		this.felicitat = felicitat;
		this.etapa = etapa;
		this.dataNaixement = dataNaixement;
	}

	@Override
	public String toString() {
		return "Tamagochi [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", gana=" + gana + ", viu=" + viu
				+ ", felicitat=" + felicitat + ", etapa=" + etapa + ", dataNaixement=" + dataNaixement + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public double getGana() {
		return gana;
	}

	public void setGana(double gana) {
		this.gana = gana;
	}

	public Boolean getViu() {
		return viu;
	}

	public void setViu(Boolean viu) {
		this.viu = viu;
	}

	public int getFelicitat() {
		return felicitat;
	}

	public void setFelicitat(int felicitat) {
		this.felicitat = felicitat;
	}

	public Etapa getEtapa() {
		return etapa;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}

	public LocalDateTime getDataNaixement() {
		return dataNaixement;
	}

}
