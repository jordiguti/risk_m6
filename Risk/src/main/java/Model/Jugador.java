package Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


//Anotacio per a marcar la classe com a entitat
@Entity
//Anotacio per indicar el nom de la taula
@Table(name = "Jugadors")
public class Jugador {
	
	// @Id ens indica que aquest atribut es una clau primaria
		@Id
		// @GeneratedValue indica a Hibernate com ha de generar la ID
		@GeneratedValue(strategy = GenerationType.AUTO)
		// @Column ens permet modificar paràmetres de la columna
		@Column(name = "id_jugador")
		private int id;
		@Column(name = "nom")
		private String nom;
		@Column(name = "numRegions")
		private int numRegions;
		@Column(name = "ordreTirada")
		private int ordreTirada;
		@Column(name = "comptadorVictories")
		private int comptadorVictories;
		
		//Continent
		/*@ManyToOne( fetch = FetchType.EAGER)
		  @JoinColumn(name="id_Regio")
		  private Regio regio;*/
		
		@OneToMany(mappedBy = "jugador", fetch = FetchType.EAGER)
		private Set<Continent> continents = new HashSet<Continent>();
		
		
		@OneToMany(mappedBy = "jugador", fetch = FetchType.EAGER)
		private Set<Regio> regions = new HashSet<Regio>();
		
		


		public int getId() {
			return id;
		}


		public void setId(int id) {
			this.id = id;
		}


		public String getNom() {
			return nom;
		}


		public void setNom(String nom) {
			this.nom = nom;
		}


		public int getNumRegions() {
			return numRegions;
		}


		public void setNumRegions(int numRegions) {
			this.numRegions = numRegions;
		}


		public int getOrdreTirada() {
			return ordreTirada;
		}


		public void setOrdreTirada(int ordreTirada) {
			this.ordreTirada = ordreTirada;
		}


		public int getComptadorVictories() {
			return comptadorVictories;
		}


		public void setComptadorVictories(int comptadorVictories) {
			this.comptadorVictories = comptadorVictories;
		}


		public Set<Continent> getContinents() {
			return continents;
		}


		public void setContinents(Set<Continent> continents) {
			this.continents = continents;
		}


		public Set<Regio> getRegions() {
			return regions;
		}


		public void setRegions(Set<Regio> regions) {
			this.regions = regions;
		}


		public Jugador() {
			super();
		}


		@Override
		public String toString() {
			return "Jugador [id=" + id + ", nom=" + nom + ", numRegions=" + numRegions + ", ordreTirada=" + ordreTirada
					+ ", comptadorVictories=" + comptadorVictories + ", continents=" + continents + ", regions="
					+ regions + "]";
		}
		
		
		
		
		
		
		
}
