package Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//Anotacio per a marcar la classe com a entitat
@Entity
//Anotacio per indicar el nom de la taula
@Table(name = "Continents")
public class Continent {
	
	// @Id ens indica que aquest atribut es una clau primaria
			@Id
			// @GeneratedValue indica a Hibernate com ha de generar la ID
			@GeneratedValue(strategy = GenerationType.AUTO)
			// @Column ens permet modificar paràmetres de la columna
			@Column(name = "id_continent")
			private int id;
			@Column(name = "nom")
			private String nom;
			@Column(name = "bonusTropes")
			private int bonusTropes;
			
			@ManyToOne( fetch = FetchType.EAGER)
			@JoinColumn(nullable=true,name="id_jugador")
			private Jugador jugador;
			
			@OneToMany(mappedBy = "continent", fetch = FetchType.EAGER)
			private Set<Regio> regions = new HashSet<Regio>();

			public int getId() {
				return id;
			}

			public void setId(int id) {
				this.id = id;
			}

			public String getNom() {
				return nom;
			}

			public void setNom(String nom) {
				this.nom = nom;
			}

			public int getBonusTropes() {
				return bonusTropes;
			}

			public void setBonusTropes(int bonusTropes) {
				this.bonusTropes = bonusTropes;
			}

			@Override
			public String toString() {
				return "Continent [id=" + id + ", nom=" + nom + ", bonusTropes=" + bonusTropes + ", jugador=" + jugador
						+ ", regions=" + regions + "]";
			}

			public Jugador getJugador() {
				return jugador;
			}

			public void setJugador(Jugador jugador) {
				this.jugador = jugador;
			}

			public Set<Regio> getRegions() {
				return regions;
			}

			public void setRegions(Set<Regio> regions) {
				this.regions = regions;
			}

			public Continent() {
				super();
			}
			
			
			

}
