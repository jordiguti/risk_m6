package Model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



//Anotacio per a marcar la classe com a entitat
@Entity
//Anotacio per indicar el nom de la taula
@Table(name = "Regions")
public class Regio {
	
	@Id
	// @GeneratedValue indica a Hibernate com ha de generar la ID
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Regio")
	private int id;
	@Column(name = "nom")
	private String nom;
	@Column(name = "numTropes")
	private int numTropes;

	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn(name="id_continent")
	private Continent continent;
	
	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn(name="id_jugador")
	private Jugador jugador;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="regio_veina", joinColumns = @JoinColumn(name="id_Regio"), inverseJoinColumns = @JoinColumn(name="vei_id"))
	private Set<Regio> veins = new HashSet<Regio>();
	
	@ManyToMany(mappedBy = "veins", cascade=CascadeType.ALL)
	private Set<Regio> veiDe = new HashSet<Regio>();
	
	public Regio() {
		super();
	}
	public Regio(String nom, int numTropes) {
		super();
		this.nom = nom;
		this.numTropes = numTropes;
	}
	public Regio(int id, String nom, int numTropes) {
		super();
		this.id = id;
		this.nom = nom;
		this.numTropes = numTropes;
	}
	
	@Override
	public String toString() {
		return "Regio [id=" + id + ", nom=" + nom + ", numTropes=" + numTropes + ", continent=" + continent
				+ ", jugador=" + jugador + ", veins=" + veins + ", veiDe=" + veiDe + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getNumTropes() {
		return numTropes;
	}
	public void setNumTropes(int numTropes) {
		this.numTropes = numTropes;
	}
	
	public Jugador getJugador() {
		return jugador;
	}
	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}
	public Set<Regio> getVeins() {
		return veins;
	}
	public void setVeins(Set<Regio> veins) {
		this.veins = veins;
	}
	public Set<Regio> getVeiDe() {
		return veiDe;
	}
	public void setVeiDe(Set<Regio> veiDe) {
		this.veiDe = veiDe;
	}
	public Continent getContinent() {
		return continent;
	}
	public void setContinent(Continent continent) {
		this.continent = continent;
	}
	
	
	
	

}
