package Model;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
//Anotacio per indicar el nom de la taula
@Table(name = "Aliments")
public class Aliment {
	@Id
	// @GeneratedValue indica a Hibernate com ha de generar la ID
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idAliment")
	private int id;
	@Column(name = "nom", length = 50, nullable = false)
	private String nom;
	@Column(name = "descripcio", length = 50, nullable = false)
	private String descripcio;
	@Column(name = "valorNutricional", columnDefinition = "double(6,2)")
	private double valorNutricional = 10.00;
	@OneToMany(mappedBy = "aliment", fetch = FetchType.EAGER)
	private Set<Tamagochi> tamagotchis = new HashSet<Tamagochi>();
	
	
	public Set<Tamagochi> getTamagotchis() {
		return tamagotchis;
	}
	public void setTamagotchis(Set<Tamagochi> tamagotchis) {
		this.tamagotchis = tamagotchis;
	}
	
	public Aliment() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescripcio() {
		return descripcio;
	}
	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}
	public double getValorNutricional() {
		return valorNutricional;
	}
	public void setValorNutricional(double valorNutricional) {
		this.valorNutricional = valorNutricional;
	}
	@Override
	public String toString() {
		return "Aliment [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", valorNutricional="
				+ valorNutricional + "]";
	}
	
	
	

}
