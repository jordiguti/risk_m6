package Main;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import Model.Aliment;
import Model.Etapa;
import Model.Joguina;
import Model.Tamagochi;

public class Main2 {
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			// exception handling omitted for brevityaa
			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}

	public static void main(String[] args) {
		// Per començar a fer operacions a la BBDD necessitem instanciar una session
		session = getSessionFactory().openSession();

		// Abans de fer cap procediment, hem de començar una transacció:
		session.beginTransaction();
		Tamagochi tama1 = new Tamagochi();
		tama1.setNom("Aurelien");
		tama1.setDescripcio("El unico frances que respeto");
		tama1.setEtapa(Etapa.Baby);
		tama1.setViu(true);
		tama1.setFelicitat(100);
		tama1.setGana(33.00);
		// gana,datanaixement, felicitat

		Tamagochi tama2 = new Tamagochi();
		tama2.setNom("Marco");
		tama2.setDescripcio("Marco fiero el mejor, Bramaluna enjoyer");
		tama2.setEtapa(Etapa.Baby);
		tama2.setViu(true);
		
		Tamagochi tama3 = new Tamagochi();
		tama3.setNom("Wolfey");
		tama3.setDescripcio("Raichu player");
		tama3.setEtapa(Etapa.Baby);
		tama3.setViu(false);
		
		Tamagochi tama4 = new Tamagochi();
		tama4.setNom("seiyun park");
		tama4.setDescripcio("Gano un fockin mundial con pachirisu");
		tama4.setEtapa(Etapa.Baby);
		tama4.setViu(false);
		
		
		Aliment a1 = new Aliment();
		a1.setNom("Cruasan");
		a1.setDescripcio("Un fockin cruasan");
		Aliment a2 = new Aliment();
		a2.setNom("Pizza con piña");
		a2.setDescripcio("Lo que realmente le gustan a los italianos");
		Aliment a3 = new Aliment();
		a3.setNom("burger cangreburger");
		a3.setDescripcio("El ingrediente secreto es el amor");
		Aliment a4 = new Aliment();
		a4.setNom("shushi");
		a4.setDescripcio("siempre entra");
		// amb el session persist fem inserts a la BBDD de nous registres
		session.persist(tama1);
		session.persist(tama2);
		session.persist(tama3);
		session.persist(tama4);
		session.persist(a1);
		session.persist(a2);
		session.persist(a3);
		session.persist(a4);
		session.getTransaction().commit();
		session.getTransaction().begin();
		
		/*Fes que els Tamagotchis es facin amics entre ells
		(com a mínim tots han de tenir un amic). 
		Guarda les dades actualitzades i fes un print de tots els registres de Tamagotchi.*/

		
		tama1.getAmics().add(tama2);
		tama2.getAmics().add(tama1);
		tama3.getAmics().add(tama4);
		tama4.getAmics().add(tama3);
		
		session.merge(tama1);
		session.merge(tama2);
		session.merge(tama3);
		session.merge(tama4);
		
		session.getTransaction().commit();
		session.getTransaction().begin();
		
		/*El Tamagotchi amb ID 1 haurà de rebre l’Aliment amb ID 3. 
		El Tamagotchi amb ID 2 haurà de rebre l’Aliment amb ID 2.
		Comprova que al Tamagotchi amb ID 3 també pot rebre l’Aliment amb ID 2. 
		Guarda les dades actualitzades.*/
		
		tama1.setAliment(a3);
		tama2.setAliment(a2);
		tama3.setAliment(a2);
		
		session.merge(tama1);
		session.merge(tama2);
		session.merge(tama3);
		
		session.merge(a3);
		session.merge(a2);
		
		session.getTransaction().commit();
		session.getTransaction().begin();
		
		List<Tamagochi> tamagochis = session.createQuery("from Tamagochi").getResultList(); 
		System.out.println(tamagochis);
		
		System.out.println();
		System.out.println();
		System.out.println();
		for(int i=0;i<tamagochis.size();i++) {
			System.out.println(tamagochis.get(i));
			System.out.println(tamagochis.get(i).getAliment());
		}
		
		
		Joguina j=new Joguina();
		//j.setId(1);
		j.setNom("Peluche dondozo");
		j.setNivellDiversio(0);
		j.setDescripcio("dondozo hijoeputa dondozo");
		
		Joguina j2=new Joguina();
		//j2.setId(2);
		j2.setNom("Peluche chien pao");
		j2.setNivellDiversio(9);
		j2.setDescripcio("chien pao o chef pao? let him cook");
		System.out.println();
		System.out.println();
		System.out.println();
		session.persist(j);
		session.persist(j2);
		
		session.getTransaction().commit();
		session.getTransaction().begin();
		
		//tama1.setJoguina(j);
		//tama2.setJoguina(j2);
		
		j.setTamagotchi(tama1);
		j2.setTamagotchi(tama2);
		
		session.merge(j2);
		session.merge(j);
		session.merge(tama1);
		session.merge(tama2);
		
		
		
		session.getTransaction().commit();
		session.getTransaction().begin();
		
		j.setTamagotchi(tama2);
		j2.setTamagotchi(tama1);
		
		session.merge(j2);
		session.merge(j);
		session.merge(tama1);
		session.merge(tama2);
		
		
		
		session.getTransaction().commit();
		session.getTransaction().begin();
		
		List<Tamagochi> tamagochis2 = session.createQuery("from Tamagochi").getResultList(); 
		System.out.println(tamagochis2);
		List<Joguina> tamagochis23 = session.createQuery("from Joguina").getResultList(); 
		System.out.println(tamagochis23);
		
		System.out.println();
		System.out.println();
		System.out.println();
		for(int i=0;i<tamagochis2.size();i++) {
			System.out.println(tamagochis2.get(i));
			System.out.println(tamagochis2.get(i).getAliment());
		}
		for(int i=0;i<tamagochis23.size();i++) {
			//if(tamagochis23.get(i).ge)) {
				System.out.println("Joguina");
				System.out.println(tamagochis23.get(i));
				System.out.println("Tamagochi de la joguina");
				System.out.println(tamagochis23.get(i).getTamagotchi());
		//	}
		}
		
		session.close();
		
		//Tamagochi tama1Id = session.find(Tamagochi.class,1);
		//System.out.println(tama1Id);
	}
}
