package DAO;

import java.io.Serializable;
import java.util.List;

public abstract class GenericDAO<T, ID extends Serializable> implements IGenericDAO<T, ID>{
	
	
	Class<T> entityClass;
	
	public T findByPK(ID pk) {
		org.hibernate.Session session = Session.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		T entity = session.find(entityClass, pk);
		session.getTransaction().commit();
		return entity;
	}

	public void deleteByPK(ID pk) {
		org.hibernate.Session session = Session.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		session.remove(findByPK(pk));
		session.getTransaction().commit();
	}

	public void delete(T entity) {
		org.hibernate.Session session = Session.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		session.remove(entity);
		session.getTransaction().commit();
	}

	public List<T> findAll() {
		org.hibernate.Session session = Session.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		List<T> reg = session.createQuery("select a from "+entityClass.getName()+" a").getResultList();
		session.getTransaction().commit();
		return reg;
	}

	public void save(T entity) {
		org.hibernate.Session session = Session.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		session.persist(entity);
		session.getTransaction().commit();
	}
	
	public void update(T entity) 
	{
		org.hibernate.Session session = Session.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		session.merge(entity);
		session.getTransaction().commit();
		
	}

	
	public void close()
	{
		org.hibernate.Session session = Session.getSessionFactory().getCurrentSession();
		session.close();
	}

}
