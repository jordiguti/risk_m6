package DAO;

import java.util.List;
import java.util.Set;

import Model.Continent;
import Model.Jugador;
import Model.Regio;

public class JugadorDAO extends GenericDAO<Jugador, Integer>
{
	public JugadorDAO() 
	{
		super.entityClass = Jugador.class;
	}
	public Jugador getJugador(int id) {
		return super.findByPK(id);
	}
	
	public void FinalitzarPartida() {
		 //Es comprova mitjançant una consulta a la BD si un jugador ja no té més regions i per tant ha perdut. 
		 //O bé, si un jugador és guanyador perquè té totes les regions. Es mostra en un text per pantalla.
		//ESTE METODO ESTA HECHO PARA QUE EL NUMERO DE REGIONES SEA 42, SI CAMBIA HAY QUE MODIFICAR EL METODO
		List<Jugador> jj = super.findAll();
		for(Jugador j : jj) {
			Set<Regio> set=j.getRegions();
			if(set.size()==0) {
				System.out.println("El jugador "+j.getNom()+" ha perdut perque te un total de "+set.size()+" regions");
			}else if(set.size()==42){
				System.out.println("El jugador "+j.getNom()+" ha guanyat perque te un total de "+set.size()+" regions");
			}
		}
	}

	public String ComptarContinents(int CodiJugador) {
		Jugador jj = super.findByPK(CodiJugador);
		String s = "El jugador amb el codi " + CodiJugador + "té el valor " + jj.getContinents().size();
		return s;
	}
	
	public String ComptarRegions(int CodiJugador) {
		Jugador jj = super.findByPK(CodiJugador);
		String s = "El jugador amb el codi " + CodiJugador + "té el valor " + jj.getRegions().size();
		return s;
	}

	// Mostra les regions associades a un jugador mitjançant una consulta a la BD.
	public void LlistarJugadorRegio(int id) {
		Jugador jj = super.findByPK(id);
		Set<Regio> cc = jj.getRegions();
		for (Regio c1 : cc) {
			System.out.println(c1.getNom());
		}
	}


	public void LlistarContinentsJugador(int idJugador) {
		Jugador jj = super.findByPK(idJugador);
		Set<Continent> cc = jj.getContinents();
		for (Continent c1 : cc) {
			System.out.println(c1.getNom());
		}
	}

}