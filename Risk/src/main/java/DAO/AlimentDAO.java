package DAO;

import java.util.List;
import java.util.Set;

import Model.Aliment;
import Model.Joguina;
import Model.Tamagochi;

public class AlimentDAO extends GenericDAO<Aliment, Integer>
{
	public AlimentDAO() 
	{
		super.entityClass = Aliment.class;
	}
	
	//Han de tenir un mètode propi que els permeti modificar el seu valorNutricional.
	
	public void updateValorNutricional(int pk,double valorNutricional) {
		super.findByPK(pk).setValorNutricional(valorNutricional);
	}
	

}