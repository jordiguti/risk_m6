package DAO;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import Model.Aliment;
import Model.Joguina;
import Model.Tamagochi;

public class TamagochiDAO extends GenericDAO<Tamagochi, Integer>
{
	public TamagochiDAO() 
	{
		super.entityClass = Tamagochi.class;
	}
	
	//Han de tenir un mètode propi que els permeti llistar tots els Tamagotchi que existeixen amb un aliment del tipus demanat.

	
	public List<Tamagochi> llistarTamagochis(Aliment a) 
	{
		List<Tamagochi> t=new ArrayList<Tamagochi>();
		List<Tamagochi> lista=super.findAll();
		for(int i=0;i<lista.size();i++) {
			System.out.println(lista.get(i));
			System.out.println(lista.get(i).getAliment());
			System.out.println(a);
			//Por algun motivo, comparando las ids funciona pero si se compara las ids si
			if(lista.get(i).getAliment().getId()==a.getId()) {
				t.add(lista.get(i));
				System.out.println("anyadido");
			}
		}
		return t;
	}
	
	
	/*
	Han de tenir un mètode per poder fer-se amics entre ells.
	Han de tenir un mètode que, si la seva gana passa de 200, automàticament utilitzen el seu aliment. Si no tenen aliment equipat, moren.
	*/
	public void limit(int pk) 
	{
		if(super.findByPK(pk).getGana()>200) {
			if(super.findByPK(pk).getAliment()!=null) {
				utilitzarAliment(pk, super.findByPK(pk).getAliment().getValorNutricional());
			}else {
				super.findByPK(pk).setViu(false);
			}
		}
	}
	
	
	public Set<Tamagochi> llistarTamagochis(int pk) 
	{
		return super.findByPK(pk).getAmics();
	}
	
	public void equiparJoguina(int pk,Tamagochi t) {
		super.findByPK(pk).getAmicDe().add(t);
	}
	
	public void utilitzarAliment(int pk, double valor) {
		Tamagochi t=super.findByPK(pk);
		if(t.getGana()-valor>=0) {
			t.setGana(t.getGana()-valor);
		}else {
			t.setGana(0);
		}
		t.setAliment(null);
	}
	
	public void utilitzarAliment(int pk,Aliment a) {
		Tamagochi t=super.findByPK(pk);
		t.setAliment(a);
		if(t.getGana()-a.getValorNutricional()>=0) {
			t.setGana(t.getGana()-a.getValorNutricional());
		}else {
			t.setGana(0);
		}
		t.setAliment(null);
	}
	
	
	public void utilitzarJoguina(int pk,Joguina j) {
		Tamagochi t=super.findByPK(pk);
		t.setJoguina(j);
		t.setFelicitat(t.getFelicitat()+j.getNivellDiversio());
		t.setJoguina(null);
	}
	
	public void equiparJoguina_I_O_Aliment(int pk,Joguina j, Aliment a) {
		if(j!=null && a!=null) {
			equiparJoguina( pk, j);
			equiparAliment( pk, a);
		}else if(j!=null && a==null) {
			equiparJoguina( pk, j);
		}else if(j==null && a!=null) {
			equiparAliment( pk, a);
		}
	}
	
	public void equiparJoguina(int pk,Joguina j) {
		super.findByPK(pk).setJoguina(j);
	}
	
	public void equiparAliment(int pk,Aliment a) {
		super.findByPK(pk).setAliment(a);
	}
	
	public void comprovarSiEstaViu(int pk,Aliment a) {
		List<Tamagochi> lista=super.findAll();
		super.findByPK(pk).setAliment(a);
		for(int i=0;i<lista.size();i++) {
			if(lista.get(i).getViu()==true) {
				System.out.println("El tamagochi "+lista.get(i).getNom()+" esta viu");
			}else {
				System.out.println("El tamagochi "+lista.get(i).getNom()+" esta mort");
			}
		}
	}
}

