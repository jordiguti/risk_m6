package DAO;

import java.io.Serializable;
import java.util.List;

public interface IGenericDAO<T, ID extends Serializable> 
{
	T findByPK(ID pk);
	void deleteByPK(ID pk);
	void delete(T entity);
	List<T> findAll();
	void save(T entity);
	void update(T entity);
}


