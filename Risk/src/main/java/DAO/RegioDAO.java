package DAO;

import java.util.ArrayList;
import java.util.List;

import Model.Continent;
import Model.Jugador;
import Model.Regio;

public class RegioDAO extends GenericDAO<Regio, Integer>
{
	public RegioDAO() 
	{
		super.entityClass = Regio.class;
	}
	public Regio get(int id) {
		return super.findByPK(id);
	}
	
	

}
