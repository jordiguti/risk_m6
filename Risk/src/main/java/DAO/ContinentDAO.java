package DAO;

import Model.Continent;

public class ContinentDAO extends GenericDAO<Continent, Integer>
{
	public ContinentDAO() 
	{
		super.entityClass = Continent.class;
	}

}