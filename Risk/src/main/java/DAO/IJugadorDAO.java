package DAO;

import java.io.Serializable;

import Model.Continent;
import Model.Jugador;

public interface IJugadorDAO<T, ID extends Serializable> extends IGenericDAO<T, ID> 
{
	Jugador get(int id);
}

