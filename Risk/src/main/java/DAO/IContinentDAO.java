package DAO;

import java.util.List;

import Model.Continent;

public interface IContinentDAO extends IGenericDAO<Continent, String> {
	
	void saveOrUpdate(Continent c);

	Continent get(String id);

	List<Continent> list();

	void delete(String id);

}
